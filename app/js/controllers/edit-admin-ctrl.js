/**
 * Created by salma on 23/8/15.
 */

App.controller('EditAdminController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    //type of users
    $scope.add_admin_img_sent= 0;
    $scope.type_of_admin=[{
        id:0,
        name: 'Admin'
    },
        {
            id:1,
            name: 'Manager'
        }
    ]
    /*--------------------------------------------------------------------------
     * ---------------- get admin's data by using admin id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/admin/getAdminUserDetails', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.admin_id
        },
        function (data) {
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                console.log(data);
                var admindetails = data.data.adminData[0]
                $scope.editadmin = {
                    user_name:admindetails.userName,
                    email_id:admindetails.email,
                    first_name:admindetails.firstName,
                    last_name:admindetails.lastName,
                    dob:admindetails.dob,
                    type_of_admin:admindetails.adminType,
                    country_code:admindetails.countryCode,
                    mobile_num:admindetails.phoneNo,
                    street_address:admindetails.streetAddress,
                    locality:admindetails.locality,
                    region:admindetails.region,
                    postal_code:admindetails.zipcode

                }
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();
        });

    /*==========================================================
     function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files) {
        console.log(files);

        $scope.add_admin_image = files[0];
        $scope.add_admin_img_sent = 1;
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("admin_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
    /*==========================================================
     api hit for add admin
     ===========================================================*/

    $scope.editAdminDetails = function(editadmin){
        console.log("here");
        if($scope.add_admin_img_sent==0){
            $scope.errorMsg = "Upload Image";
            $scope.TimeOutError();
            return false;
        }
        console.log(editadmin.type_of_admin.id);


        var formData = new FormData();
        formData.append('accessToken', $cookieStore.get('obj').accesstoken);
        formData.append('userName', editadmin.user_name);
        formData.append('userId', $stateParams.admin_id);
        formData.append('email', editadmin.email_id);
        formData.append('password', editadmin.password);
        formData.append('firstName', editadmin.first_name);
        formData.append('lastName', editadmin.last_name);
        formData.append('dob', editadmin.dob);
        formData.append('adminType', editadmin.type_of_admin.id);
        formData.append('countryCode', editadmin.country_code);
        formData.append('phoneNo', editadmin.mobile_num);
        formData.append('address', editadmin.street_address);
        formData.append('locality', editadmin.locality);
        formData.append('region', editadmin.region);
        formData.append('zipcode', editadmin.postal_code);
        formData.append('admin_image',$scope.add_admin_image);

        //api hit here

        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/api/admin/editAdminUser',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Admin Details Updated Successfully.";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            }
        });
    }

    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }

});



