
App.controller('PaymentController', function ($scope, $http, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';
    $scope.showloader=true;

    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("payment.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };
    //var payment_details = function () {
        $.post(MY_CONSTANT.url + '/driver_payment_details', {
            access_token: $cookieStore.get('obj').accesstoken

        }, function (data) {
            //console.log(data);
            $scope.showloader=false;
            var dataArray = [];
            var excelArray = [];
            data = JSON.parse(data);

            if (data.status == responseCode.SUCCESS) {
                var driverList = data.data.driver_data;
                driverList.forEach(function (column) {
                    var e = {}
                    e.Driver_ID = column.driver_id;
                    e.Driver_Name = column.user_name;
                    e.Driver_email = column.user_email;
                    e.Driver_Phone = column.phone_no;
                    e.Total_Earning_Commission = column.total_earning_commision;
                    e.Total_earnings = column.total_earnings;
                    e.Paid_payments = column.driver_paid_payments;
                    //e.Cash_earnings = column.driver_cash_earnings;
                    e.Remaining_Amount = column.remaining_amount_paid_to_driver;

                    excelArray.push(e);

                    var d = {

                    };

                    d.driver_id = column.driver_id;
                    d.user_name = column.user_name;
                    d.user_email = column.user_email;
                    d.phone_no = column.phone_no;
                    d.total_earnings = column.total_earnings;
                    d.total_earning_commission = column.total_earning_commision;
                    d.paid_payments = column.driver_paid_payments;
                    d.actual_remaining_Amount = column.remaining_amount_paid_to_driver;
                    d.remaining_Amount = column.remaining_amount_paid_to_driver;
                    d.cash_earnings = column.driver_cash_earnings;
                    dataArray.push(d);

                });

                $scope.$apply(function () {
                    $scope.list = dataArray;
                    $scope.excelList = excelArray;

                    // Define global instance we'll use to destroy later
                    var dtInstance;

                    $timeout(function () {
                        if (!$.fn.dataTable) return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true,  // Table pagination
                            'ordering': true,  // Column ordering
                            'info': true,  // Bottom left status text
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [6,7] }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });
            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }


        });


    /*--------------------------------------------------------------------------
     * --------- funtion for pay amount ----------------------------------------
     --------------------------------------------------------------------------*/
    $scope.pay = function (driverId,actual_remaining_Amount, amount) {

        var textNumber = /((\d+)((\.\d{1,2})?))$/;

        if(amount > actual_remaining_Amount || !(textNumber.test(amount)) || amount == "" || amount < 0){
            $scope.displaymsg = "Please enter valid amount";
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        }
        else {
            $.post(MY_CONSTANT.url + '/payDriver',
                {
                    access_token: $cookieStore.get('obj').accesstoken,
                    driver_id: driverId,
                    amount: amount
                }, function (data) {
                    data = JSON.parse(data);

                    if (data.status == responseCode.SUCCESS) {
                        $scope.displaymsg = "Payment done successfully.";
                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }
                    else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                        $state.go('page.login');
                    }
                    else{
                        $scope.displaymsg = "Something went wrong.";
                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }
                    $scope.$apply();
                    //payment_details();
                });
        }
    };

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };
});


