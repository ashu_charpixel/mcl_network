/**
 * Created by salma on 22/8/15.
 */
/**
 * Created by Salma on 8/18/15.
 */
App.controller('EditCustomerController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    //type of users
    $scope.add_driver_img_sent= 0;
    $scope.type_of_admin=[{
        id:0,
        name: 'Sub Admin'
    },
        {
            id:1,
            name: 'Staff'
        }
    ]
    /*==========================================================
     function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files) {
        console.log(files);

        $scope.edit_driver_image = files[0];
        $scope.edit_driver_img_sent = 1;
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("driver_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
    /*--------------------------------------------------------------------------
     * ---------------- get driver's data by using driver id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/customers/customer', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.customer_id
        },
        function (data) {
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                var customerLIst = data.data.customer[0];
                console.log(customerLIst);
                $scope.editcustomer = {
                    email_id:customerLIst.email,
                    firstname:customerLIst.firstName,
                    lastname:customerLIst.lastName,
                    acc_num:customerLIst.accountNo,
                    routing_num:customerLIst.routingNo,
                    dob:customerLIst.dob,
                    country_code:customerLIst.countryCode,
                    mobile_num:customerLIst.phoneNo,
                    street_address:customerLIst.streetAddress,
                    //locality:,
                    //region:,
                    postal_code:customerLIst.zipcode,
                    type_of_vehicle:parseInt(customerLIst.carType)
                }
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();
        });
    /*==========================================================
     api hit for add admin
     ===========================================================*/

    $scope.editCustomerSubmit = function(editcustomer){


        //api hit here
        $.post(MY_CONSTANT.url + '/api/customers/update',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: $stateParams.customer_id,
                firstName:editcustomer.firstname,
                lastName:editcustomer.lastname
            }, function (data) {
                // data = JSON.parse(data);
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Customer Details Updated successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                console.log($scope.list);
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });

    }

    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }

});



