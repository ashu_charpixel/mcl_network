App.controller('SurgePricingController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,responseCode,$timeout,ngDialog) {

    $scope.select_car = '';
    $scope.show_table=false;
    $scope.showvalue=1;
    $scope.editvalue=0;
    $scope.showupdate=0;
    $.post(MY_CONSTANT.url + '/list_all_cars', {access_token: $cookieStore.get('obj').accesstoken},
        function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            if (data.status== responseCode.SUCCESS) {
                var carList = data.data.car_list;
                $scope.select_car = data.data.car_list[0].car_type;
                // $scope.car_type =data.data.car_id;

                console.log("select"+ $scope.select_car);
                carList.forEach(function (column) {
                    var d = {
                        car_type: "",
                        car_name: ""
                        // car_id: "",
                        // fare_fixed: "",
                        // fare_per_km: "",
                        // fare_per_min: ""
                    };
                    d.car_type = column.car_type;
                    d.car_name = column.car_name;
                    //d.car_id = column.car_id;
                    //d.fare_fixed = column.fare_fixed;
                    //d.fare_per_km = column.fare_per_km;
                    //d.fare_per_min = column.fare_per_min;
                    //d.wait_time_fare_per_min=column.wait_time_fare_per_min;
                    dataArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.list = dataArray;
                    scrollTo(0, 0);
                });
            }
            else{
                $state.go('page.login');
            }
        });

    /*--------------------------------------------------------------------------
     *----------------- funtion when car type change ---------------------------
     --------------------------------------------------------------------------*/
    $scope.set = function (car_type) {
        console.log("type"+car_type);


        $.post(MY_CONSTANT.url + '/request_fare_details', {access_token: $cookieStore.get('obj').accesstoken,
                car_type:car_type},

            function (data) {
                var dataArray = [];
                data = JSON.parse(data);
                console.log(data.data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.show_table=true;
                    console.log(data.data);

                    var cartypelist = data.data;
                    cartypelist.forEach(function (column) {
                        var d = {
                            id: "",
                            start_slot: "",
                            end_slot: "",
                            fare_fixed: "",
                            fare_per_min:"",
                            fare_per_km: "",
                            wait_time_fare_per_min: ""

                        };

                        d.id = column.id;
                        d.start_slot = column.start_slot;
                        d.end_slot = column.end_slot;
                        d.fare_fixed = column.fare_fixed;
                        //d.fare_per_min=column.fare_fixed;
                        //d.fare_per_km = column.fare_fixed;
                        //d.wait_time_fare_per_min = column.fare_fixed;

                        d.fare_per_min=column.fare_per_min;
                        d.fare_per_km = column.fare_per_km;
                        d.wait_time_fare_per_min = column.wait_time_fare_per_min;




                        dataArray.push(d);
                    });

                    $scope.$apply(function () {
                        $scope.cartypes = dataArray;

                        // Define global instance we'll use to destroy later
                        var dtInstance;

                        $timeout(function () {
                            if (!$.fn.dataTable)
                                return;
                            dtInstance = $('#datatable2').dataTable({
                                'paging': true, // Table pagination
                                'ordering': true, // Column ordering
                                'info': true,// Bottom left status text
                                'retrieve': true,
                                // Text translation options
                                // Note the required keywords between underscores (e.g _MENU_)
                                oLanguage: {
                                    sSearch: 'Search all columns:',
                                    sLengthMenu: '_MENU_ records per page',
                                    info: 'Showing page _PAGE_ of _PAGES_',
                                    zeroRecords: 'Nothing found - sorry',
                                    infoEmpty: 'No records available',
                                    infoFiltered: '(filtered from _MAX_ total records)'
                                }
                            });
                            var inputSearchClass = 'datatable_input_col_search';
                            var columnInputs = $('tfoot .' + inputSearchClass);

                            // On input keyup trigger filtering
                            columnInputs
                                .keyup(function () {
                                    dtInstance.fnFilter(this.value, columnInputs.index(this));
                                });
                        });
                        // When scope is destroyed we unload all DT instances
                        // Also ColVis requires special attention since it attaches
                        // elements to body and will not be removed after unload DT
                        $scope.$on('$destroy', function () {
                            dtInstance.fnDestroy();
                            $('[class*=ColVis]').remove();
                        });
                    });
                }



                else{
                    console.log(data.message);
                    $scope.errorMsg=data.message;
                    setTimeout(function () {
                        $scope.errorMsg = "";
                        $scope.$apply();
                    }, 3000);
                }

            });
    }

    $scope.car = {};

    $scope.EditableInput=function(){
        console.log("here");
        $scope.showvalue=0;
        $scope.editvalue=1;
        $scope.showupdate=1;

    }
    $scope.SubmitUpdatedFare=function(data){
        console.log(data);
        for(i=0;i<data.length;i++){
            data[i].fare_per_km=data[i].fare_fixed;
            data[i].fare_per_min=data[i].fare_fixed;
            data[i].wait_time_fare_per_min=data[i].fare_fixed;
        }

        console.log("data");
        console.log(data);
        var updatedFare =  JSON.stringify(data);


        $.post(MY_CONSTANT.url + '/update_fare_details',  { fare_update :updatedFare, access_token:$cookieStore.get('obj').accesstoken }
        ).then(
            function (data) {
                data = JSON.parse(data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Fares Updated Successfully. "

                }
                else {
                    $scope.displaymsg = data.message;
                }
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false,
                    showClose:false
                })

            });

    }

    /*--------------------------------------------------------------------------
     * ---------------- funtion for update car fare ----------------------------
     --------------------------------------------------------------------------*/
    $scope.addCarFare = function (cars) {
        $scope.successMsg = '';
        $scope.errorMsg = '';
        $scope.car.access_token = $cookieStore.get('obj').accesstoken;
        $scope.car.car_id = $scope.select_car;
        $scope.car.fare_fixed = cars.fare_fixed;
        $scope.car.fare_per_km = cars.fare_per_km;
        $scope.car.fare_per_min = cars.fare_per_min;

        $.post(MY_CONSTANT.url + '/update_car_fare', $scope.car
        ).then(
            function (data) {
                data = JSON.parse(data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.successMsg = data.message.toString();
                    $scope.promo = {};
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.successMsg = "";
                        $scope.$apply();
                    }, 3000);
                } else {
                    $scope.errorMsg = data.message.toString();
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.errorMsg = "";
                        $scope.$apply();
                    }, 3000);
                }
                scrollTo(0, 0);
            });
    };

});
