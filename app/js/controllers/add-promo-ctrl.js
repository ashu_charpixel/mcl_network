App.controller('AddPromoController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode) {

    $scope.show_perc_off = false;
    $scope.show_max_off = false;
    $scope.promo_img_sent = 0;
    $scope.cropper = {};
    $scope.cropper.sourceImage = null;
    $scope.cropper.croppedImage   = null;
    $scope.bounds = {};
    $scope.bounds.left = 0;
    $scope.bounds.right = 0 ;
    $scope.bounds.top = 0;
    $scope.bounds.bottom = 0;
    $scope.showCroppingArea = 0;
    $scope.left = 0;
    $scope.right = 0;

    //flag value for vat registered
    $scope.discount_type = [{
        id:1,
        name: 'Fixed Discount'
    },
        {
            id:2,
            name: '% Discount'
        },
        {
            id:3,
            name: '% Discount with fixed Maximum Discount'
        },


    ];

    $scope.promo = {};
    $scope.displaymsg = '';

    $scope.minDate = new Date();


    $scope.today = function() {
        $scope.promo.start_date = new Date();
    };

    $scope.clear = function () {
        $scope.promo.start_date = null;
    };

    //// Disable weekend selection
    //$scope.disabled = function(date, mode) {
    //    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    //};

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
        $scope.opened1=false;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy/MM/dd';



    $scope.today = function() {
        $scope.promo.end_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.promo.end_date = null;
    };

    //// Disable weekend selection
    //$scope.disabled = function(date, mode) {
    //    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    //};

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
        $scope.opened = false;

    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy/MM/dd';
    /*======================================================================
     *==================function to upload image =======================
     =====================================================================*/
    $scope.file_to_upload = function (files) {
        console.log(files);
        $scope.showCroppingArea = 1;
        $scope.promo_code_image=files[0];
        $scope.promo_img_sent = 1;

        $scope.bounds.left = 0;
        $scope.bounds.right = 0 ;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;

        $scope.left = 0;
        $scope.right = 0;

        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("promo_code_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);


    }

    /*=========================================================================
     *=========function to check bounds of cropped image ======================
     =========================================================================*/
    $scope.$watch('cropper.croppedImage', function (newValue, oldValue) {

        if ($scope.left == 0 && $scope.bounds.left != 0) {
            console.log("Change left");
            $scope.left = $scope.bounds.left;
        }
        if($scope.right == 0 && $scope.bounds.right !=0){
            console.log("Change right");
            $scope.right =$scope.bounds.right
        }

    });

    /*=========================================================================
     *=========function to show fields according to type selected  ===========
     =========================================================================*/
    $scope.discountShow = function(type){
        console.log("type"+type);
        if(type==1){
            $scope.show_max_off = true;
            $scope.show_perc_off =false;

        }
        if(type==2){
            $scope.show_perc_off =true;
            $scope.show_max_off = false;



        }
        if(type==3){
            $scope.show_max_off = true;
            $scope.show_perc_off =true;


        }
    }

    /*--------------------------------------------------------------------------
     * ---------------- function to add promo code -----------------------------
     --------------------------------------------------------------------------*/
    $scope.addPromoCode = function () {

        $scope.successMsg = '';
        $scope.errorMsg = '';

        console.log($scope.promo.discount_type);
        console.log($scope.promo.max_off);


        var start_date = $scope.promo.start_date1;
        var end_date = $scope.promo.end_date1;
        start_date = new Date(start_date);
        end_date = new Date(end_date);

        if(end_date)
            end_date.setDate(end_date.getDate() + 1);

        var days = end_date - start_date;

        if($scope.promo.start_date1 == '' || $scope.promo.start_date1 == undefined || $scope.promo.start_date1 == null){
            $scope.errorMsg = "Please select Start Date";
            $scope.TimeOutError();
            return false;
        }
        if($scope.promo.end_date1 == '' || $scope.promo.end_date1 == undefined || $scope.promo.end_date1 == null){
            $scope.errorMsg = "Please select End Date";
            $scope.TimeOutError();
            return false;
        }
        if (days <= 0) {
            $scope.errorMsg = "Start Date must be less than End Date";
            $scope.TimeOutError();
            return false;
        }
        if($scope.promo_img_sent ==0){
            $scope.errorMsg = "Select Image for Promo Code";
            $scope.TimeOutError();
            return false;
        }


        if($scope.promo.discount_type==1 && ($scope.promo.max_off =='' || $scope.promo.max_off== undefined))
        {
            $scope.errorMsg = "Enter Maximum Discount";
            $scope.TimeOutError();
            return false;

        }
        if($scope.promo.discount_type==2 && ($scope.promo.perc_off =='' ||$scope.promo.perc_off==undefined))
        {
            $scope.errorMsg = "Enter (%) Discount";
            $scope.TimeOutError();
            return false;

        }
        if($scope.promo.discount_type==3 && (($scope.promo.perc_off =='' || $scope.promo.max_off =='') || ($scope.promo.perc_off ==undefined || $scope.promo.max_off ==undefined)))
        {
            $scope.errorMsg = "Enter Maximum & (%) Discount";
            $scope.TimeOutError();
            return false;

        }
        if($scope.cropper.croppedImage){

            $scope.croppedimg =  window.dataURLtoBlob($scope.cropper.croppedImage);
        }
        else{
            console.log("no cropping");
            $scope.croppedimg = $scope.promo_code_image;
        }
        if( $scope.bounds.left == $scope.left && $scope.bounds.right == $scope.right ){
            $scope.croppedimg = $scope.promo_code_image;
        }



        start_date = $("#start_date").val();
        end_date = $("#end_date").val();
        start_date = start_date + " 00:00:00";
        var start_date = new Date(start_date);
        console.log("new"+start_date);
        start_date = start_date.toUTCString();
        console.log(start_date);

        end_date = end_date + " 23:59:00";
        end_date = new Date(end_date);
        end_date = end_date.toUTCString();
        console.log(end_date);


        $scope.promo.start_date = start_date;
        $scope.promo.end_date = end_date;
        var formData = new FormData();
        formData.append('access_token', $cookieStore.get('obj').accesstoken);
        formData.append('promotion_code', $scope.promo.promo_code);
        formData.append('days_validity', $scope.promo.validity);
        formData.append('num_coupons', $scope.promo.num_coupons);
        formData.append('start_date', start_date);
        formData.append('end_date', end_date);
        formData.append('description', $scope.promo.description);
        formData.append('promo_image_flag', $scope.promo_img_sent);
        formData.append('promo_image', $scope.croppedimg);
        formData.append('type', $scope.promo.discount_type);
        formData.append('no_coupons_per_user', $scope.promo.num_coupons_per_user);
        if($scope.promo.discount_type==1){
            formData.append('discount', '100%');
            formData.append('maximum', $scope.promo.max_off);
        }
        else if($scope.promo.discount_type==2){
            formData.append('discount', $scope.promo.perc_off);
            formData.append('maximum', '1000000');
        }
        else{
            formData.append('discount', $scope.promo.perc_off);
            formData.append('maximum', $scope.promo.max_off);
        }

        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/insert_promo_details',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Promo Code Added Successfully.";

                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else if(data.error){
                    $scope.displaymsg = data.error;
                }
                else {
                    $scope.displaymsg = data.message;
                }
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            }
        });

    };


    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.go('app.form');
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };




    //animation of text area

    $("#description").focus(function() {

        $(this).animate({
            height: 100
        }, "normal"),



            $(this).blur(function() {

                $(this).animate({
                    height: 35
                }, "normal")

            });
    });

    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }

});

