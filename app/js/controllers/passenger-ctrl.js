/**
 * Created by sanjay on 3/28/15.
 */
App.controller('PassengerDatabaseController', function ($scope, $http,$route,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog,responseCode) {

    'use strict';
    $scope.showloader=true;

    $scope.delete_passenger_id = '';
    $scope.displaymsg = '';
    $scope.driverpass ={}

    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("passengers.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

        $.post(MY_CONSTANT.url + '/api/customers/listCustomers', {
            accessToken: $cookieStore.get('obj').accesstoken

        }, function (data) {
            $scope.showloader=false;
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }

            var dataArray = [];
            var excelArray = [];
            if (data.flag == responseCode.SUCCESS){
                var customerList = data.data.customer;
                customerList.forEach(function (column) {
//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                    var e={}
                    e.USER_ID = column.userId;
                    e.NAME = column.firstName + " " + column.lastName;
                    e.RATINGS = column.totalRating+"/5";
                    e.EMAIL = column.email;
                    e.CONTACT = column.phoneNo;
                    e.isLogin = (column.isLogin ==1)?'YES':'NO';
                    e.registered = "-";
                    e.is_Blocked = (column.isBlocked ==1)?'BLOCKED':'UNBLOCKED';
                    e.is_Deleted = (column.isDeleted ==0)?'NO':'YES';
                    excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                    var d = {};
                    if(column.isDeleted != 1){
                        d.user_id = column.userId;
                        d.name = column.firstName + " " + column.lastName;
                        d.ratings = column.totalRating;
                        d.user_email = column.email;
                        d.phone_no = column.phoneNo;
                        d.isLogin = column.isLogin;
                        d.registered = "-";
                        d.is_blocked = column.isBlocked;
                        d.is_deleted = (column.isDeleted ==0)?'NO':'YES';
                        d.viewcustomer_url = "/app/view-customer/" +  column.userId;
                        d.editcustomer_url = "/app/edit-customer/" +  column.userId;
                        dataArray.push(d);
                    }

                });

                $scope.$apply(function () {
                    $scope.list = dataArray;
                    $scope.excelList=excelArray;

                    // Define global instance we'll use to destroy later
                    var dtInstance;

                    $timeout(function () {
                        if (!$.fn.dataTable) return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true,  // Table pagination
                            'ordering': true,  // Column ordering
                            'info': true,  // Bottom left status text
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [6] }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });

                    // When scope is destroyed we unload all DT instances
                    // Also ColVis requires special attention since it attaches
                    // elements to body and will not be removed after unload DT
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });
            }
           else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }

        });

    /*--------------------------------------------------------------------------
     * ------------- funtion to open modal for delete passenger ----------------
     --------------------------------------------------------------------------*/
    $scope.deletepassenger_popup = function (delete_passenger_id) {

        $scope.delete_passenger_id = delete_passenger_id;

        ngDialog.open({
            template: 'delete_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };


    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete passenger --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_passenger = function (delete_passenger_id) {

        console.log('delete pop up ');
        console.log('delete_passenger_id ' + delete_passenger_id);

        ngDialog.close({
            template: 'delete_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/api/customers/delete',{
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: delete_passenger_id
            }, function (data) {

            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
                console.log(data);

            $scope.displaymsg = '';

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Passenger is deleted successfully.";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
            });
    };


    /*--------------------------------------------------------------------------
     * --------- funtion to open dialog for block or unblock passenger ------------
     --------------------------------------------------------------------------*/
    $scope.blockunblockpassenger_popup = function (is_blocked,user_id) {
        console.log('is_blocked '+is_blocked);
        console.log('user_id '+user_id);

        $scope.blockunblockmsg = '';
        $scope.blockunblockid = user_id;
        $scope.is_blocked = is_blocked;

        if (is_blocked == 1) {
            $scope.blockunblockmsg = "Are you sure you want to block this passenger?";
        } else {
            $scope.blockunblockmsg = "Are you sure you want to unblock this passenger?";
        }

        ngDialog.open({
            template: 'block_unblock_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ------------------ funtion to block or unblock passenger -------------------
     --------------------------------------------------------------------------*/
    $scope.do_block_unblock_passenger = function (is_blocked,blockunblockid) {
        console.log('is_blocked '+is_blocked);
        console.log('blockunblockid '+blockunblockid);

        $.post(MY_CONSTANT.url + '/api/customers/blockUnblock',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId": blockunblockid,
                "blockStatus": is_blocked
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    if (is_blocked == 1) {
                        $scope.displaymsg = "Passenger is blocked successfully.";
                    } else {
                        $scope.displaymsg = "Passenger is unblocked successfully.";
                    }
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.close({
                    template: 'block_unblock_passenger_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });

                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

 //==============================================================================================================================
//==========================================================password dialog-====================================================
//==============================================================================================================================
    $scope.openPasswordDialog = function(id){

        $scope.driver_pass_id = id
        ngDialog.open({
            template: 'password_dialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }
    //==============================================================================================================================
//==========================================================password dialog-====================================================
//==============================================================================================================================
    $scope.changeDriverPass = function(driver){

        $.post(MY_CONSTANT.url + '/api/customers/changePassword',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: $scope.driver_pass_id,
                password:driver.password

            }, function (data) {

                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.driverpass.successMsg = "Password Changed Successfully.";
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.driverpass.successMsg = "";
                        $scope.$apply();
                        ngDialog.close({
                            template: 'password_dialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }, 3000);
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.driverpass.errorMsg = data.error;
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.driverpass.errorMsg = "";
                        $scope.$apply();
                        ngDialog.close({
                            template: 'password_dialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }, 3000);
                }
            });

    }

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        console.log("Refresh page");
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };


});

