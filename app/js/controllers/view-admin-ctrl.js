/**
 * Created by salma on 23/8/15.
 */

App.controller('ViewAdminController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    /*--------------------------------------------------------------------------
     * ---------------- get driver's data by using driver id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/admin/getAdminUserDetails', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.admin_id
        },
        function (data) {
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                var adminList = data.data.adminData[0]
                console.log(adminList);
                $scope.user_name = adminList.firstName + " " +adminList.lastName ;
                $scope.phone_no = adminList.phoneNo;
                $scope.email = adminList.email;
                $scope.address = adminList.streetAddress + "," + adminList.locality + ","+ adminList.region + ", Zipcode:" + adminList.zipcode;
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();
        });
});




