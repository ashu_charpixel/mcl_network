/**
 * Created by sanjay on 3/25/15.
 */
App.controller('LoginController', function ($rootScope,$scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog) {
    //initially set those objects to null to avoid undefined error
    // place the message if something goes wrong
    $scope.stayLoggedIn = true;
     $scope.account = {};
    $scope.account.email = "";
    $scope.account.password = "";
    $scope.authMsg = '';

    if($.cookie("rememberEmail")){
        $scope.account.email = $.cookie("rememberEmail");
    }

    $scope.loginAdmin = function () {

        if($scope.account.email == "" || $scope.account.password == ""){}
       else {
            console.log("done");
        $scope.authMsg = '';
        $.post(MY_CONSTANT.url + '/api/admin/login',
            {
                email: $scope.account.email,
                password: $scope.account.password
            }).then(
            function (data) {
                //data = JSON.parse(data);

                if (data.flag == 200) {
                    if ($scope.stayLoggedIn) {
                        console.log(" data.data.adminData.email"+ data.data.adminData.email);
                        $.cookie("rememberEmail", data.data.adminData.email, { expires: 365 });

                    }
                    var someSessionObj = {'accesstoken': data.data.accessToken};
                    $cookieStore.put('obj', someSessionObj);
                    //$cookieStore.put('type',data.data.type);
                    //console.log($cookieStore.get('type'));
                    $state.go('app.dashboard');
                }
                else if(data.error){
                    console.log("error");
                    $scope.authMsg = data.error;
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.authMsg = "";
                        $scope.$apply();
                    }, 3000);
                }
                else
                {
                    $scope.authMsg = data.error;
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.authMsg = "";
                        $scope.$apply();
                    }, 3000);
                }





            });
    }
    };

    $scope.recover = function () {

        $.post(MY_CONSTANT.url + '/forgot_password',
            {
                email: $scope.account.email
            }).then(
            function (data) {
                data = JSON.parse(data);
                console.log(data);
                if (data.status == 200) {
                    $scope.successMsg = "Please check your email to reset password.";
                    setTimeout(function () {
                        $scope.successMsg = "";
                        $scope.$apply();
                    }, 3000);
                } else {
                    $scope.errorMsg = data.message.toString();
                    $scope.successMsg = data.message.toString();
                    setTimeout(function () {
                        $scope.errorMsg = "";
                        $scope.$apply();
                    }, 3000);

                }
                $scope.$apply();
            })
    };

    $scope.logout = function () {
        ngDialog.open({
            template: 'logout_dialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    }

    $scope.logoutFunction = function () {
        ngDialog.close({
            template: 'logout_dialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
        $cookieStore.remove('obj');
        $cookieStore.remove('zoom');
        $cookieStore.remove('type');
        $state.go('page.login');
    }
});

