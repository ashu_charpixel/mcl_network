/**
 * Created by salma on 9/4/16.
 */
/**
 * Created by salma on 8/4/16.
 */
/**
 * Created by sanjay on 3/28/15.
 */
App.controller('contactUsController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode) {

    'use strict';
    $scope.exportData = function () {

        console.log($scope.excelList);
        alasql('SELECT * INTO CSV("drivers.csv",{headers:true}) FROM ?', [ $scope.excelList]);
    }
    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/api/customers/contactUsList', {
        accessToken: $cookieStore.get('obj').accesstoken
    }, function (data) {
        console.log(data);
        $scope.showloader=false;
        var dataArray = [];
        var excelArray=[];


        if(typeof data === "string")
        {
            data = JSON.parse(data);
        }

        if (data.flag == responseCode.SUCCESS) {
            var driverList = data.data.contactData;
            driverList.forEach(function (column) {

//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e ={}
                e.USER_ID = column.id;
                e.USER_NAME = column.name;
                e.EMAIL = column.email;
                e.CONTACT = column.countryCode+"-"+column.phoneNo;
                e.Comment = column.commentText;
                e.USERTYPE = column.userType;
                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel ========================================
//==========================================================================================================================
                var d = {};
                if(column.isResolved === 0){
                    d.id = column.id;
                    d.name = column.name;
                    d.email = column.email;
                    d.contact = column.countryCode+'-'+column.phoneNo;
                    d.commentText  = column.commentText;
                    d.userType  = column.userType;
                    dataArray.push(d);
                }

            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList = excelArray;


                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [] }
                        ],
                        //"aaSorting": [[0,"desc"]]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });
                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }
    });

    /*--------------------------------------------------------------------------
     * --------- funtion to add reply to customer -----------------------------
     --------------------------------------------------------------------------*/
    $scope.openReplyBox = function(data){
        $scope.id = data.id;
        ngDialog.open({
            template: 'reply_modal',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    }
   $scope.sendReply = function(data){
       ngDialog.close({
           template: 'reply_modal',
           className: 'ngdialog-theme-default',
           scope: $scope
       });
       $.post(MY_CONSTANT.url + '/api/customers/sendReplyForContactUs',
           {
               accessToken: $cookieStore.get('obj').accesstoken,
               id: $scope.id,
               subject:data.subject,
               content:data.content
           }, function (data) {
               if(typeof data == "string")
               {
                   data = JSON.parse(data);
               };
               console.log(data);

               if (data.flag == responseCode.SUCCESS) {
                   $scope.displaymsg = "Reply sent successfully";
               }
               else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                   $state.go('page.login');
               }
               else {
                   $scope.displaymsg = data.error;
               }
               $scope.$apply();
               ngDialog.open({
                   template: 'display_msg_modalDialog',
                   className: 'ngdialog-theme-default',
                   showClose: false,
                   scope: $scope
               });
           });
   }







      /*--------------------------------------------------------------------------
     * --------- funtion to open confirmation for add--------------------------
     --------------------------------------------------------------------------*/
    $scope.confirmPopUp = function(data){
        $scope.id = data.id;
        ngDialog.open({
            template: 'confirmResolve',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    }
    $scope.markResolve = function(id){
        ngDialog.close({
            template: 'confirmResolve',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/customers/markContactUsQueryResolved',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                id: id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                };
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Query resolved successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    }
    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

});