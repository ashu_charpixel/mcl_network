App.controller('EmailTemplateController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $timeout, $state, responseCode, ngDialog, $stateParams) {
    $scope.template = {};
    $scope.updateFlag = false;
    $scope.sendMail = {};
    $scope.sendMail.flag = 0;
    if ($stateParams.templateId) {
        //get info of particular template
        $.post(MY_CONSTANT.url + '/api/emailTemplates/getTemplateList',
            {
                accessToken: $cookieStore.get('obj').accesstoken
            },
            function (response) {
                console.log(response);
                //if (response.flag== responseCode.SUCCESS) {
                //    var templates = response.data.templates;
                //    templates.forEach(function (column) {
                //        var d = {};
                //        d.id = column.id;
                //        d.name = column.name;
                //        d.updateUrl =  "/app/updateEmailTemplate/" +  column.id;
                //        dataArray.push(d);
                //    });
                //    $scope.$apply(function () {
                //        $scope.list = dataArray;
                //        scrollTo(0, 0);
                //    });
                //}
                //else{
                //    //$state.go('page.login');
                //}

            });

    }

    var dataArray = [];
    $scope.viewAllTemplate = function () {
        $.post(MY_CONSTANT.url + '/api/emailTemplates/getTemplateList',
            {
                accessToken: $cookieStore.get('obj').accesstoken
            },
            function (response) {
                console.log(response);
                if (response.flag == responseCode.SUCCESS) {
                    var templates = response.data.templates;
                    templates.forEach(function (column) {
                        var d = {};
                        d.id = column.id;
                        d.name = column.name;
                        d.htmlContent = column.htmlContent;
                        d.updateUrl = "/app/updateEmailTemplate/" + column.id;
                        dataArray.push(d);
                    });
                    $scope.$apply(function () {
                        $scope.list = dataArray;
                        scrollTo(0, 0);
                        // Define global instance we'll use to destroy later
                        var dtInstance;

                        $timeout(function () {
                            if (!$.fn.dataTable)
                                return;
                            dtInstance = $('#datatable2').dataTable({
                                'paging': true, // Table pagination
                                'ordering': true, // Column ordering
                                'info': true, // Bottom left status text
                                // Text translation options
                                // Note the required keywords between underscores (e.g _MENU_)
                                oLanguage: {
                                    sSearch: 'Search all columns:',
                                    sLengthMenu: '_MENU_ records per page',
                                    info: 'Showing page _PAGE_ of _PAGES_',
                                    zeroRecords: 'Nothing found - sorry',
                                    infoEmpty: 'No records available',
                                    infoFiltered: '(filtered from _MAX_ total records)'
                                },
                                "aoColumnDefs": [
                                    {'bSortable': false, 'aTargets': []}
                                ]
                            });
                            var inputSearchClass = 'datatable_input_col_search';
                            var columnInputs = $('tfoot .' + inputSearchClass);

                            // On input keyup trigger filtering
                            columnInputs
                                .keyup(function () {
                                    dtInstance.fnFilter(this.value, columnInputs.index(this));
                                });
                        });
                    });
                }
                else {
                    //$state.go('page.login');
                }

            });
    }

    /*-------------------------------------------------------------------------
     -------------------show div for upade template  ---------------------------
     --------------------------------------------------------------------------*/
    $scope.updateEmailTempShow = function (data) {
        console.log(data);
        $scope.template = {
            name: data.name,
            email_template_html: data.htmlContent,
            id: data.id
        }
        $scope.updateFlag = true;
    }
    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete template------------
     --------------------------------------------------------------------------*/
    $scope.deleteEmailTemplatePopup = function (id) {

        $scope.template_id = id;

        ngDialog.open({
            template: 'deleteTemplateConfirm',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };
    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete promo code -------------------
     --------------------------------------------------------------------------*/
    $scope.deleteTemplate = function (id) {

        ngDialog.close({
            template: 'deleteTemplateConfirm',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/api/emailTemplates/deleteTemplate',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                id: id
            }, function (data) {
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Template deleted successfully.";
                }

                else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion for update/add template ----------------------------
     --------------------------------------------------------------------------*/
    $scope.updateEmailTemplate = function (template, flag) {

        $scope.successMsg = '';
        $scope.errorMsg = '';
        $scope.template.accessToken = $cookieStore.get('obj').accesstoken;
        $scope.template.name = template.name;
        $scope.template.htmlContent = template.email_template_html;
        var url = "";
        if (flag == 0) {  //add
            url = '/api/emailTemplates/addNewTemplate';
        }
        else if (flag == 1) {  //update
            $scope.template.id = template.id;
            url = '/api/emailTemplates/updateTemplate';

        }
        $.post(MY_CONSTANT.url + url, $scope.template
        ).then(
            function (data) {
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.successMsg = data.message;
                    if(flag == 0)
                    $scope.template = {};
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.successMsg = "";
                        if(flag == 1){
                            //$scope.updateFlag = false;

                        }
                        $scope.$apply();

                    }, 3000);
                    scrollTo(0, 0);
                } else {
                    $scope.errorMsg = data.error;
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.errorMsg = "";
                        $scope.$apply();
                    }, 3000);
                }
                scrollTo(0, 0);
            });
    };
    /*--------------------------------------------------------------------------
    ----------------------function to open pop up for send mail ----------------
     --------------------------------------------------------------------------*/
    $scope.sendMailPopup = function(data){
        console.log(data);
        $scope.sendMail.templateId = data.id;
        ngDialog.open({
            template: 'sendMailPopup',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };
    $scope.sendMailSubmit = function(data){
        console.log(data);
        $scope.sendMail.errorMsg = "Sending... "
        //0 for all 1 for selected
        $scope.sendMail.accessToken = $cookieStore.get('obj').accesstoken;
        $scope.sendMail.subject = data.subject;
        if(data.flag == 0){
            $scope.sendMail.userId = -1;
        }
        if(data.flag == 0){
            $.post(MY_CONSTANT.url + '/api/emailTemplates/sendEmail', $scope.sendMail
            ).then(
                function (data) {
                    console.log(data);
                    if (data.flag == responseCode.SUCCESS) {
                        $scope.sendMail.errorMsg ="";
                        $scope.sendMail.successMsg = data.message;
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.sendMail.successMsg = "";
                            $scope.$apply();
                        }, 3000);
                        scrollTo(0, 0);
                    } else {
                        $scope.sendMail.errorMsg = data.error;
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.sendMail.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                    scrollTo(0, 0);
                });
        }
    }

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

});
