/**
 * Created by Salma on 8/18/15.
 */
App.controller('AddDriverController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode) {
    //type of users
    $scope.add_driver_img_sent= 0;
    $scope.add_driver_car_img_sent= 0;
    $scope.adddriver = {};
    $scope.adddriver.countryCode = 1;
    //$scope.$apply();
    var dtInstance;
    var d = new Date();
    $scope.offset = d.getTimezoneOffset();

    //getting car types in list
    $.post(MY_CONSTANT.url + '/api/vehicles/viewType', {accessToken: $cookieStore.get('obj').accesstoken},
        function (data) {
            var carArray = [];
            if(typeof(data)=='string'){
                data = JSON.parse(data);
            }
            console.log("viewtype");
            console.log(data);

            if (data.flag == responseCode.SUCCESS) {
                var carList = data.data.vehicles;
                carList.forEach(function (column) {
                    var d = {};
                    d.name = column.name;
                    d.id = column.id;
                    d.type = column.type;
                    carArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.carlist = carArray;
                });
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                $state.go('page.login');
            }
        });
    /*==========================================================
     function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files,flag) {
        if(flag==0){
            $scope.userImage = files[0];
            $scope.add_driver_img_sent = 1;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("driver_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }
        else if(flag==1){
            $scope.driverCarImage = files[0];
            $scope.add_driver_car_img_sent = 1;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("vehicle_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }

    }
    /*==========================================================
     api hit for add admin
     ===========================================================*/

    $scope.addDriver = function(adddriver){
        //if($scope.add_driver_img_sent==0){
        //    $scope.errorMsg = "Upload  User Image";
        //    $scope.TimeOutError();
        //    return false;
        //}
        //if($scope.add_driver_car_img_sent==0){
        //    $scope.errorMsg = "Upload Vehicle Image";
        //    $scope.TimeOutError();
        //    return false;
        //}
        var formData = new FormData();
        formData.append('accessToken', $cookieStore.get('obj').accesstoken);
        formData.append('email', adddriver.email);
        formData.append('firstName', adddriver.firstName);
        formData.append('lastName', adddriver.lastName);
        formData.append('countryCode', "+"+ adddriver.countryCode);
        formData.append('phoneNo', adddriver.phoneNo);
        formData.append('dob', adddriver.dob);
        formData.append('timeOffset', $scope.offset);
        formData.append('drivingLicenseNo', adddriver.driverLicenseNo);
        formData.append('driverCarImage', $scope.driverCarImage);
        formData.append('userImage', $scope.userImage);
        formData.append('carType', adddriver.carType);
        formData.append('carModel', adddriver.carModel);
        formData.append('carNo', adddriver.carNo);
        formData.append('streetAddress', adddriver.streetAddress);
        formData.append('city', " ");
        formData.append('state', " ");
        formData.append('zipcode', adddriver.zipCode);
        formData.append('accountNo', adddriver.accountNo);
        formData.append('routingNo', adddriver.routingNo);
        formData.append('permitNo', adddriver.permitNo);
        formData.append('carYear', adddriver.carYear);

        //api hit here
        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/api/drivers/addDriver',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                if(typeof(data)=="string"){
                    data = JSON.parse(data);
                }
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver Added Successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                        $scope.errorMsg = data.error;
                        $scope.TimeOutError();
                        return false;
                }


            },
            error:function(data){
                if(typeof(data)=="string"){
                    data = JSON.parse(data)
                }
                var response = JSON.parse(data.responseText);
                $scope.errorMsg = response.error;
                $scope.TimeOutError();
                return false;
            }
        });
    }

    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }

});


