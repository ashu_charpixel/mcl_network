/**
 * Created by click89 on 7/14/15.
 */
/**
 * Created by sanjay on 3/28/15.
 */
App.controller('EngagementController', function ($scope, $http,$route,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog,responseCode,$stateParams) {

    'use strict';
    $scope.showloader=true;

    $scope.delete_passenger_id = '';
    $scope.displaymsg = '';

    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("engagementlist.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

    $.post(MY_CONSTANT.url + '/list_engage_details', {
        access_token: $cookieStore.get('obj').accesstoken,
        promo_code:$stateParams.promo_code


    }, function (data) {
        $scope.showloader=false;
        //console.log(data);
        var dataArray = [];
        var excelArray = [];
        data = JSON.parse(data);
        console.log(data);

        if (data.status == responseCode.SUCCESS){

            var engagementList = data.data

            engagementList.forEach(function(column) {

//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e={}
                e.Engagement_Id = column.engagement_id;
                e.Pick_Up_Location = column.pickup_location_address;
                e.Drop_Off_Location = column.drop_location_address;
                e.Net_Amount = column.net_pay;
                e.Discount = column.discount;
                e.Paid_Amount = column.to_pay

                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                var d = { };

                d.engagement_id = column.engagement_id;
                d.pick_up_location = column.pickup_location_address;
                d.drop_off_location = column.drop_location_address;
                d.net_amount = column.net_pay;
                d.discount = column.discount;
                d.paid_amount = column.to_pay

                dataArray.push(d);
            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList=excelArray;

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable) return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true,  // Table pagination
                        'ordering': true,  // Column ordering
                        'info': true,  // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }

    });




});


