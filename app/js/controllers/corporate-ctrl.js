/**
 * Created by sanjay on 3/28/15.
 */
App.controller('CorporateController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode) {

    'use strict';
    $scope.showloader=true;



    $scope.addcorporateAdmin = {};
    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("corporate.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/list_all_coraporate_admins', {
        access_token: $cookieStore.get('obj').accesstoken
    }, function (data) {
        $scope.showloader=false;

        var dataArray = [];
        var excelArray =[];
        data = JSON.parse(data);

        if (data.status == responseCode.SUCCESS) {

            var cList = data.data.admin_list;
            cList.forEach(function (column) {
                //==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e={}

                e.Corporate_ID = column.user_id;
                e.Corporate_name = column.user_name;
                e.Corporate_email = column.email;
                e.Phone_no = column.phone_no;
                e.Total_Money = column.total_money;
                e.Money_Left = column.money_left;
                e.Active_Members = column.active_members;

                e.Account_Name =column.account_name;
                e.Contact_Name = column.contact_name;
                e.Contact_Address=column.contact_address;
                e.PO_Number = column.po_number;



                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                var d = {

                };

                d.user_id = column.user_id;
                d.user_name = column.user_name;
                d.email = column.email;
                d.phone_no = column.phone_no;
                d.total_money = column.total_money;
                d.money_left = column.money_left;
                d.active_members = column.active_members;
                d.isblock = column.isblock;
                d.account_name =column.account_name;
                d.contact_name = column.contact_name;
                d.contact_address=column.contact_address;
                d.po_number = column.po_number;
                d.invoice_list_url = "/app/invoice-list/" +column.user_id





                dataArray.push(d);
            });

            $scope.$apply(function () {
                $scope.corporatelist = dataArray;
                $scope.excelList = excelArray;

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [10,11,12,13] }
                        ]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });
                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
            $state.go('page.login');
        }
        else {
            alert(data.message);
        }
    });

    /*--------------------------------------------------------------------------
     * -------------------funtion to open dialog for add corporate account ----------------
     --------------------------------------------------------------------------*/
    $scope.AddCorporateDialog = function () {

        ngDialog.open({
            template: 'modalDialogId',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };


    /*============================================================================
     ==========================funtion to add CORPORATE aCCOUNT==============================
     ============================================================================*/
    $scope.submit = function (addcorporateAdmin) {

        $.post(MY_CONSTANT.url + '/add_corporate_admin', {
            access_token: $cookieStore.get('obj').accesstoken,
            email: addcorporateAdmin.email,
            user_name: addcorporateAdmin.name,
            phone_no: "+" + addcorporateAdmin.phone_no,
            account_name: addcorporateAdmin.account_name,
            contact_name: addcorporateAdmin.contact_name,
            contact_address: addcorporateAdmin.contact_address,
            po_number: addcorporateAdmin.po_number


        }, function (data) {
            console.log(data);
            data = JSON.parse(data);

            if (data.status == responseCode.SUCCESS) {
                $scope.addcorporateAdmin.successMsg = "Corporate Admin Successfully Added.";
                $scope.$apply();
                setTimeout(function () {
                    $scope.addcorporateAdmin.successMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalDialogId',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
                $state.reload();
            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                $state.go('page.login');
            }
            else {
                $scope.addcorporateAdmin.errorMsg = data.message;
                $scope.$apply();
                setTimeout(function () {
                    $scope.addcorporateAdmin.errorMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalDialogId',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
            }
        })
    };

    /*============================================================================
     ==========================funtion to do payment==============================
     ============================================================================*/
    $scope.pay = function (id,email, amount) {

        $.post(MY_CONSTANT.url + '/generate_corp_admin_invoice', {
            access_token: $cookieStore.get('obj').accesstoken,
            corp_admin_ID: id,
            corp_admin_email: email
        }, function (data) {
            console.log(data);
            data = JSON.parse(data);

            if (data.status == responseCode.SUCCESS) {
                $scope.displaymsg = data.message;
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false,
                    showClose: false
                })

            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                $state.go('page.login');
            }
            else if(data.error){
                $scope.displaymsg = data.error;
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false,
                    showClose: false
                })

            }
            else {
                $scope.displaymsg = data.message;
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false,
                    showClose: false
                })

            }
        })
    }


    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * --------- funtion to open dialog for block or unblock driver ------------
     --------------------------------------------------------------------------*/
    $scope.blockunblockdriver_popup = function (is_blocked, user_id) {

        $scope.blockunblockmsg = '';
        $scope.blockunblockid = user_id;
        $scope.is_blocked = is_blocked;

        if (is_blocked == 1) {
            $scope.blockunblockmsg = "Are you sure you want to block this corporate admin?";
        } else {
            $scope.blockunblockmsg = "Are you sure you want to unblock this corporate admin?";
        }

        ngDialog.open({
            template: 'block_unblock_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ------------------ funtion to block or unblock driver -------------------
     --------------------------------------------------------------------------*/
    $scope.do_block_unblock_driver = function (is_blocked, blockunblockid) {
        console.log('is_blocked ' + is_blocked);
        console.log('blockunblockid ' + blockunblockid);

        $.post(MY_CONSTANT.url + '/Block_Unblock_corp_admin',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                corp_admin_ID: blockunblockid,
                IsBlock: is_blocked
            }, function (data) {
                data = JSON.parse(data);

                if (data.status == responseCode.SUCCESS) {
                    if (is_blocked == 1) {
                        $scope.displaymsg = "Corporate Admin is blocked successfully.";
                    } else {
                        $scope.displaymsg = "Corporate Admin is unblocked successfully.";
                    }
                }
                else {
                    $scope.displaymsg = data.message;
                }
                $scope.$apply();
                ngDialog.close({
                    template: 'block_unblock_driver_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });

                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false

                });
            });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete driver ----------------
     --------------------------------------------------------------------------*/
    $scope.deletecorp_dialog = function (delete_corp_id) {

        console.log('delete function');
        console.log(delete_corp_id);

        $scope.delete_corp_id = delete_corp_id;

        ngDialog.open({
            template: 'delete_corp_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.deletecorp_admin = function (delete_corp_id) {

        ngDialog.close({
            template: 'delete_corp_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/delete_corp_admin',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                corp_admin_ID: delete_corp_id,
                isdelete: 1
            }, function (data) {
                data = JSON.parse(data);
                console.log(data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Corporate Admin Deleted Successfully.";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.message;
                }

                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope,
                    closeByDocument: false,
                    closeByEscape: false

                });
            });


    };



});