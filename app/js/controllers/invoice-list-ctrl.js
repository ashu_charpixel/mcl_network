/**
 * Created by click89 on 7/14/15.
 */
/**
 * Created by sanjay on 3/28/15.
 */
App.controller('InvoiceListController', function ($scope, $http,$route,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog,responseCode,$stateParams) {

    'use strict';
    $scope.showloader=true;

    $scope.delete_passenger_id = '';
    $scope.displaymsg = '';

    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("invoicelist.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

    $.post(MY_CONSTANT.url + '/list_invoice_details', {
        access_token: $cookieStore.get('obj').accesstoken,
        corp_admin_id:$stateParams.corp_id


    }, function (data) {
        $scope.showloader=false;
        //console.log(data);
        var dataArray = [];
        var excelArray = [];
        data = JSON.parse(data);
        console.log(data);

        if (data.status == responseCode.SUCCESS){

            var engagementList = data.data

            engagementList.forEach(function(column) {

//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e={}
                e.Invoice_Id = column.invoice_id;
                e.Start_Date = moment(column.start_date).format("DD-MM-YYYY HH:mm:ss");
                e.End_Date = moment(column.end_date).format("DD-MM-YYYY HH:mm:ss");
                e.Sent_Date = moment(column.sent_date).format("DD-MM-YYYY HH:mm:ss");
                e.Running_Balance = column.payable_amount;


                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                var d = { };

                d.invoice_id = column.invoice_id;
                d.start_date = moment(column.start_date).format("DD-MM-YYYY HH:mm:ss");
                d.end_date = moment(column.end_date).format("DD-MM-YYYY HH:mm:ss");
                d.sent_date = moment(column.sent_date).format("DD-MM-YYYY HH:mm:ss");
                d.payable_amount = column.payable_amount;
                d.invoice_view_url = column.invoice_url


                dataArray.push(d);
            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList=excelArray;

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable) return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true,  // Table pagination
                        'ordering': true,  // Column ordering
                        'info': true,  // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        }
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }

    });




});


